Router.configure({
    layoutTemplate: 'main'
});

Router.route('/', {
template: 'userOverview',
});

Router.route('register');
Router.route('login');
Router.route('examOverview');
Router.route('caseStudy1');
Router.route('caseStudy1Questions');
Router.route('caseStudy1Review');
Router.route('userOverview');
Router.route('caseStudy2');
Router.route('caseStudy2Questions');
Router.route('section2FirstLot');
Router.route('caseStudy2Review');
Router.route('section2LastLot');
Router.route('finalReview');