Meteor.methods({
  //create a new past paper if user has requested one and is logged in.
  addPastPaper: function(){
    if (! Meteor.userId()) {
    throw new Meteor.Error("not-authorized"); }   
    UserPastPapers.insert({userId: Meteor.userId(), pastPaperName: "Nat 5 Business Management", completed: false, overallMark: [], percentage: "0%", createdAt: new Date()});
  },
  //insert the questions for the first case study
  insertCaseStudy1Questions: function(){
  if (CaseStudy1Questions.find().count() == 0){     
  CaseStudy1Questions.insert({question1APart1: "From the case study, identify 2 enterprising skills or qualities that Caroline has demonstrated."});
  CaseStudy1Questions.insert({question1APart2: "Outline how these skills or qualities help Caroline develop her business."});
  CaseStudy1Questions.insert({question1b: "From the case study, compare the 2 types of finance provided by the PSYBT"});
  CaseStudy1Questions.insert({question1c: "Caroline provides a service to her customers. Justify the importance of providing good customer service."});
  CaseStudy1Questions.insert({question1Dpart1: "Caroline employed a member of staff. Outline 3 stages in the recruitment process"});
  CaseStudy1Questions.insert({question1Dpart2: "Describe the features of the Equality Act 2010."});  
  CaseStudy1Questions.insert({question1Epart1: "From the case study, identify the stage of the product life cycle for Caroline’s business."});
  CaseStudy1Questions.insert({question1Epart2: "Describe the stage identified in Question E part 1"});
  }},
  //insert the answer hints for the case study 1 review page if user gets any of the questions wrong
  insertCaseStudy1AnswerHints: function(){
  if (CaseStudy1AnswerHints.find().count() == 0){
  CaseStudy1AnswerHints.insert({question1APart1: "You could say that Caroline has experience in dog grooming"});
  CaseStudy1AnswerHints.insert({question1APart2: "She has experience in dog grooming therefore she will be able to meet customer needs"});
  CaseStudy1AnswerHints.insert({question1b: "You could state that they are both types of finance from external sources"});
  CaseStudy1AnswerHints.insert({question1c: "You could mention that because of the good customer service, customers will recommend to friends/family"});
  CaseStudy1AnswerHints.insert({question1D: "You could state something simple such as, advertise the job"});
  CaseStudy1AnswerHints.insert({question1E: "You could put either growth or maturity as stages of the product life cycle"});  
  }  
  },
  //once user has finished the first case study, update the past paper that the user is currently on with the users answers
  updatePastPaperWithQ1Answers: function(question1Total, pastPaperId, firstSkill, secondSkill, howDoesFirstSkillHelp,
                                         howDoesSecondSkillHelp, onetypeOfFinance, anotherTypeOfFinance,
                                         oneImportanceOfGoodCustomerService, anotherImportanceOfGoodCustomerService, oneStageOfRecruitmentProcess,
                                         anotherStageOfRecruitmentProcess, andAnotherStageOfRecruitmentProcess,
                                         oneFeatureOfEqualityAct, anotherFeatureOfEqualityAct, productLifeCycleStage,
                                         describeStage, percentage){ 
  //update the current users past paper with their answers and overall mark to the first case study
    UserPastPapers.update({ _id: pastPaperId },{ userId: Meteor.userId(), pastPaperName: "Nat 5 Business Management", completed: false, completedOn: new Date(), overallMark: question1Total, 
                                                firstSkill: firstSkill, secondSkill: secondSkill, howDoesFirstSkillHelp: howDoesFirstSkillHelp, howDoesSecondSkillHelp: howDoesSecondSkillHelp, 
                                                onetypeOfFinance: onetypeOfFinance, anotherTypeOfFinance: anotherTypeOfFinance, oneImportanceOfGoodCustomerService: oneImportanceOfGoodCustomerService, 
                                                anotherImportanceOfGoodCustomerService: anotherImportanceOfGoodCustomerService, oneStageOfRecruitmentProcess: oneStageOfRecruitmentProcess, 
                                                anotherStageOfRecruitmentProcess: anotherStageOfRecruitmentProcess, andAnotherStageOfRecruitmentProcess: andAnotherStageOfRecruitmentProcess, 
                                                oneFeatureOfEqualityAct: oneFeatureOfEqualityAct, anotherFeatureOfEqualityAct: anotherFeatureOfEqualityAct, productLifeCycleStage: productLifeCycleStage, 
                                                describeStage: describeStage, percentage: percentage });
  },
  insertCaseStudy2Questions: function(){
  if (CaseStudy2Questions.find().count() == 0){
  CaseStudy2Questions.insert({question2a: "Compare the objectives of The Big Issue, identified from the case study, with those of a public sector organisation."});
  CaseStudy2Questions.insert({question2bPart1: "From the case study, identify the method of promotion that is used by The Big Issue."});
  CaseStudy2Questions.insert({question2bPart2: "Describe other methods of promotion that could be used by The Big Issue"});
  CaseStudy2Questions.insert({question2c: "Explain how external factors could affect the success of The Big Issue"});
  CaseStudy2Questions.insert({question2dPart1: "The Big Issue could use rail to deliver its magazines to its distribution points nationwide, Identify another method of distribution"});
  CaseStudy2Questions.insert({question2dPart2: "State the advantages and disadvantages of this method."});
  CaseStudy2Questions.insert({question2e: "Describe the factors to be considered when setting the price for The Big Issue."});  
  }  
  },
  updatePastPaperWithQ2Answers: function(overallTotal, pastPaperId, compareObjectives, methodOfPromotion, describeOtherMethods, externalFactors, anotherMethodOfDistribution, advantagesAndDisAdvantages, 
                                         settingPriceFactors, percentage){
    UserPastPapers.update({ _id: pastPaperId,  userId: Meteor.userId() }, {$set :{overallMark: overallTotal, compareObjectives: compareObjectives, methodOfPromotion: methodOfPromotion, 
                                                describeOtherMethods: describeOtherMethods, externalFactors: externalFactors, anotherMethodOfDistribution: anotherMethodOfDistribution,
                                                advantagesAndDisAdvantages: advantagesAndDisAdvantages, settingPriceFactors: settingPriceFactors, percentage: percentage}});
  },
  insertSection2FirstLot: function(){
  if (Section2FirstLot.find().count() == 0){
  Section2FirstLot.insert({question3a: "Discuss the advantages and disadvantages of recycling to an organisation."});
  Section2FirstLot.insert({question3b: "Explain the problems of having too much stock."});
  Section2FirstLot.insert({question3cPart1: "The quality of products is important to all businesses. Identify 2 methods of ensuring quality"});
  Section2FirstLot.insert({question3cPart2: "Describe the methods identified in (c)(i)."});
  Section2FirstLot.insert({question4a: "Define the following financial terms. Fixed Costs, Variable Costs and Sales Revenue"});
  Section2FirstLot.insert({question4b: "Describe the actions that can be taken by an organisation to reduce costs."});
  Section2FirstLot.insert({question4cPart1: "Justify the use of a spreadsheet in the finance department."});
  Section2FirstLot.insert({question4cPart2: "Describe the ways that other software can be used in the finance apartment"});  
  }  
  },
  updatePastPaperWithSection2FirstLotAnswers: function(overallTotal, pastPaperId, question3a, question3b, question3cPart1, question3cPart2, 
                question4a, question4b, question4cPart1, question4cPart2, percentage){
    UserPastPapers.update({ _id: pastPaperId, userId: Meteor.userId() }, {$set :{overallMark: overallTotal, question3a: question3a, question3b: question3b, question3cPart1: question3cPart1,
                                                                         question3cPart2: question3cPart2, question4a: question4a, question4b: question4b, question4cPart1: question4cPart1,
                                                                         question4cPart2: question4cPart2, percentage: percentage}});
  },
  insertSection2LastLot: function(){
  if (Section2LastLot.find().count() == 0){
  Section2LastLot.insert({question5a: "Describe the selection process used to choose the right person for the job."});
  Section2LastLot.insert({question5bPart1: "Outline 2 methods of industrial action."});
  Section2LastLot.insert({question5bPart2: "Explain the impact of industrial action on an organisation."});
  Section2LastLot.insert({question5c: "Compare piece-rate with time-rate as methods of calculating wages."});
  Section2LastLot.insert({question6a: "Outline 2 internal factors that can affect the success of an organisation."});
  Section2LastLot.insert({question6b: "Describe factors of production."});
  Section2LastLot.insert({question6cPart1: "Identify 2 stakeholders of a supermarket."});
  Section2LastLot.insert({question6cPart2: "Explain how these stakeholders could influence the success of the organisation."}); 
  }  
  },
  completePastPaper: function(overallTotal, pastPaperId, question5a, question5bPart1, question5bPart2, question5c, question6a, question6b, question6cPart1, question6cPart2, percentage){
    UserPastPapers.update({ _id: pastPaperId, userId: Meteor.userId() }, {$set :{overallMark: overallTotal, question5a: question5a, question5bPart1: question5bPart1, question5bPart2: question5bPart2,
                                                                                question5c: question5c, question6a: question6a, question6b: question6b, question6cPart1: question6cPart1,
                                                                                question6cPart2: question6cPart2, percentage: percentage, completed: true}});
  }
})