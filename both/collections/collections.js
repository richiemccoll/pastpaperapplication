CaseStudy1Questions = new Mongo.Collection('caseStudy1Questions');
CaseStudy1AnswerHints = new Mongo.Collection('caseStudy1AnswerHints');
CaseStudy2Questions = new Mongo.Collection('caseStudy2Questions');
UserPastPapers = new Mongo.Collection('userPastPapers');
Section2FirstLot = new Mongo.Collection('section2FirstLot');
Section2LastLot = new Mongo.Collection('section2LastLot');

Meteor.startup(function() {
if (CaseStudy1Questions.find().count() == 0){  
Meteor.call('insertCaseStudy1Questions');
}
if (CaseStudy1AnswerHints.find().count() == 0){
Meteor.call('insertCaseStudy1AnswerHints');  
}
if (CaseStudy2Questions.find().count() == 0){
Meteor.call('insertCaseStudy2Questions');  
}
if (Section2FirstLot.find().count() == 0){
Meteor.call('insertSection2FirstLot');  
}
if (Section2LastLot.find().count() == 0){
Meteor.call('insertSection2LastLot');  
} 
});