This is an application that mimics a past paper from the 2014 business management nat 5 exam.
The reason for building this was to give my younger brother (who is sitting his nat 5's this year) a different option to help him study.

At the moment I have the full 2014 past paper working along with the marking logic which is checking against keywords from the official marking scheme.

The same logic follows throughout the application. With my brother and his classmates using this on the testing server to help them study they will let me know bugs or faults in the logic that I can fix.

Application is live on a testing server at:

pastpaperwebapplication.meteor.com

