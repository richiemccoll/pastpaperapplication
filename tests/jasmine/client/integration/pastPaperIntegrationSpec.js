'use strict'
describe ('PastPaper', function(){
it ('should be created by a logged in user', function(done){
// login to system and wait for callback
Meteor.loginWithPassword("test@test.com", "testpassword", function(err){
// check if we have correctly logged in the system
expect(err).toBeUndefined();

//create a new past paper
var pp = new PastPaper();
  
//save the past paper and use callback function to check for existence  
var id = pp.save(function(error, result) {
expect(error).toBeUndefined();
  
Meteor.logout(function() {
done();
})  
});                
});  
});  
});