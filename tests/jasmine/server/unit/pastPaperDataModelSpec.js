'use strict'
describe ('PastPaper', function(){
it ("should be created with a past paper name", function(){
  spyOn(PastPapers, "insert").and.callFake(function(doc, callback){
    // simulate async return of id = "1";
    callback(null, "1");
  });
  
  var pastPaper = new PastPaper(null, "Nat 5 Business Management");
  
  expect(pastPaper.name).toBe("Nat 5 Business Management");
  
  pastPaper.save();
  
  // id should be defined
  expect(pastPaper.id).toEqual("1");
  expect(PastPapers.insert).toHaveBeenCalledWith({name: "Nat 5 Business Management"}, jasmine.any(Function));
});  
});