Meteor.publish('userPastPapers', function(){
  return UserPastPapers.find();
});

Meteor.publish('caseStudy1Questions', function(){
  return CaseStudy1Questions.find();
});

Meteor.publish('caseStudy1AnswerHints', function(){
  return CaseStudy1AnswerHints.find();
})

Meteor.publish('caseStudy2Questions', function(){
  return CaseStudy2Questions.find();
});

Meteor.publish('section2FirstLot', function(){
  return Section2FirstLot.find();
})

Meteor.publish('section2LastLot', function(){
  return Section2LastLot.find();
})