Template.section2LastLot.helpers({
  section2LastLot: function(){
    return Section2LastLot.find();
  }
})

Template.section2LastLot.onCreated(function(){
   if (! Meteor.userId()){
    Router.go('login');
  }
  else {
  Session.setDefault("secondLotCounter2", 0);
  }
});

Template.section2LastLot.events({
  'click .button.success': function(event){
    //define the counter variables
    var c = 0; var d = 0; var e = 0;
    var f = 0; var g = 0; var h = 0;
    var i = 0; var j = 0;
    
//define keyword arrays
    var question5aKeywordArray = ['application', 'CV', 'suitable', 'applicants', 'compare', 'specification', 'references', 'interviews', 'questions', 'potential', 'testing',
                                 'suitability', 'attainment', 'aptitude', 'intelligence', 'successful', 'unsuccessful'];
    var question5bPart1KeywordArray = ['sit-in', 'remain', 'workplace', 'overtime', 'ban', 'rule', 'description', 'slow', 'rate', 'strike', 'refuse', 'demonstrations', 'picket',
                                      'withdrawal', 'lock', 'premises', 'relocated', 'closed', 'boycott'];
    var question5bPart2KeywordArray = ['production', 'halt', 'struggle', 'demand', 'elsewhere', 'damage', 'reputation', 'delays', 'loss', 'revenue', 'refusing', 'slow', 'reputation',
                                      'share', 'price', 'fall', 'recruit', 'difficult', 'potential'];
    var question5cKeywordArray = ['units', 'hour', 'sales', 'made', 'higher', 'suffer', 'quantity', 'standard', 'output', 'unskilled', 'factory', 'skilled'];
    var question6aKeywordArray = ['finance', 'staff', 'time', 'experience', 'training', 'equipment', 'technology', 'products', 'leadership', 'management'];
    var question6bKeywordArray = ['land', 'natural', 'resources', 'labour', 'workplace', 'wages', 'capital', 'resources', 'premises', 'equipment', 'machinery', 'invested', 
                                 'organisation', 'capital', 'interest', 'enterprise', 'idea', 'profit', 'factors', 'production'];
    var question6cPart1KeywordArray = ['owners', 'shareholders', 'employees', 'managers', 'suppliers', 'lendors', 'creditors', 'government', 'community', 'customers', 'pressure'];
    var question6cPart2KeywordArray = ['decisions', 'mistakes', 'resulting', 'profit', 'investment', 'impact', 'vary', 'complaints', 'wastage', 'industrial', 'amount', 'supplies', 'final', 
                                      'delay', 'halt', 'interest', 'affordable', 'repayment', 'outflows', 'legislation', 'implement', 'council', 'policies', 'restrictions', 'planning',
                                      'permission', 'protest', 'questions', 'influence', 'image', 'custom', 'elsewhere'];
    
//get user input and store in meaningful variable names
//convert to lower case so that input can match the keyword array and replace any malicious chars before input into the database    
    var question5a = $('input[name=question5a]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var question5bPart1 = $('input[name=question5bPart1]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var question5bPart2 = $('input[name=question5bPart2]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var question5c = $('input[name=question5c]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var question6a = $('input[name=question6a]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var question6b = $('input[name=question6b]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var question6cPart1 = $('input[name=question6cPart1]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var question6cPart2 = $('input[name=question6cPart2]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    
//define marking functions that can be used over and over again 
//also handy that if any of the logic needs changed which it no doubt will, only have to change it in the one place.
//first function is used to mark the questions that can award up to 2 marks  
//second function is used to mark the questions that can award only 1 mark 
//third function is used to mark the questions that can award up to 3 marks 
//forth function is used to mark the questions that can award up to 4 marks 
    var markTheQuestion = function(c, userInput, keywordArray){
    var c = c;
    var userInput = userInput;
    var keywordArray = keywordArray;  
    
    var splitUserInput = userInput.split(" ");
    
    $.each(keywordArray, function(indexA, valueA){
    $.each(splitUserInput, function(indexB, valueB){
    if(valueA == valueB){
    //if the value is found, log to the console that we have a match, add 1 to the variable C declared at the start and then break out of the loop.
    console.log('we have 1 skilled match');
    c++;     
    return false;        
    } 
    });  
    })
    
    //if c is not equal to 0 then set the session counter to 1, meaning that 1 mark has been given for this question.
    if (c >= 2){Session.set("secondLotCounter2", Session.get("secondLotCounter2") + 2);}
    else if (c != 2 && c != 0){Session.set("secondLotCounter2", Session.get("secondLotCounter2") + 1);}  
    };
   
    var markTheQuestion2 = function(c, userInput, keywordArray){
    var c = c;
    var userInput = userInput;
    var keywordArray = keywordArray;  
    
    var splitUserInput = userInput.split(" ");
    
    $.each(keywordArray, function(indexA, valueA){
    $.each(splitUserInput, function(indexB, valueB){
    if(valueA == valueB){
    //if the value is found, log to the console that we have a match, add 1 to the variable C declared at the start and then break out of the loop.
    console.log('we have 1 skilled match');
    c++;     
    return false;        
    } 
    });  
    })
    
    //if c is not equal to 0 then set the session counter to 1, meaning that 1 mark has been given for this question.
    if (c != 0){Session.set("secondLotCounter2", Session.get("secondLotCounter2") + 1);}  
    };
    
    var markTheQuestion3 = function(c, userInput, keywordArray){
    var c = c;
    var userInput = userInput;
    var keywordArray = keywordArray;  
    
    var splitUserInput = userInput.split(" ");
    
    $.each(keywordArray, function(indexA, valueA){
    $.each(splitUserInput, function(indexB, valueB){
    if(valueA == valueB){
    //if the value is found, log to the console that we have a match, add 1 to the variable C declared at the start and then break out of the loop.
    console.log('we have 1 skilled match');
    c++;     
    return false;        
    } 
    });  
    })
    
    //if c is not equal to 0 then set the session counter to 1, meaning that 1 mark has been given for this question.
    if (c >= 3){Session.set("secondLotCounter2", Session.get("secondLotCounter2") + 3);}
    else if (c != 3 && c == 2){Session.set("secondLotCounter2", Session.get("secondLotCounter2") + 2);}
    else if (c != 3 && c != 2 && c != 0){Session.set("secondLotCounter2", Session.get("secondLotCounter2") + 1);}  
    };
    
    var markTheQuestion4 = function(c, userInput, keywordArray){
    var c = c;
    var userInput = userInput;
    var keywordArray = keywordArray;  
    
    var splitUserInput = userInput.split(" ");
    
    $.each(keywordArray, function(indexA, valueA){
    $.each(splitUserInput, function(indexB, valueB){
    if(valueA == valueB){
    //if the value is found, log to the console that we have a match, add 1 to the variable C declared at the start and then break out of the loop.
    console.log('we have 1 skilled match');
    c++;     
    return false;        
    } 
    });  
    })
    
    //if c is not equal to 0 then set the session counter to 1, meaning that 1 mark has been given for this question.
    if (c >= 4){Session.set("secondLotCounter2", Session.get("secondLotCounter2") + 4);}
    else if (c < 4 && c > 2){Session.set("secondLotCounter2", Session.get("secondLotCounter2") + 3);}
    else if (c < 4 && c != 3 && c == 2){Session.set("secondLotCounter2", Session.get("secondLotCounter2") + 2);}
    else if (c < 4 && c != 3 && c != 2 && c != 0){Session.set("secondLotCounter2", Session.get("secondLotCounter2") + 1);}  
    };  
    
//mark the questions and insert answers into database
    
    markTheQuestion4(c, question5a, question5aKeywordArray);
    markTheQuestion(d, question5bPart1, question5bPart1KeywordArray);
    markTheQuestion(e, question5bPart2, question5bPart2KeywordArray);
    markTheQuestion(f, question5c, question5cKeywordArray);
    markTheQuestion(g, question6a, question6aKeywordArray);
    markTheQuestion3(h, question6b, question6bKeywordArray);
    markTheQuestion(i, question6cPart1, question6cPart1KeywordArray);
    markTheQuestion3(j, question6cPart2, question6cPart2KeywordArray);
    
    var userID = Meteor.userId();
    var section2LastLotTotal = Session.get("secondLotCounter2");
    var percentage = "100%";
    Session.set("currentTotal", Session.get('currentTotal') + section2LastLotTotal);
    var pastPaperId = Session.get('currentPastPaperId');
    var overallTotal = Session.get('currentTotal');
    
    Meteor.call('completePastPaper', overallTotal, pastPaperId, question5a, question5bPart1, question5bPart2, question5c, question6a, question6b, question6cPart1, question6cPart2, percentage);
    
    Meteor.defer(function() { Router.go('finalReview'); }) 
  }
});