Template.section2FirstLot.helpers({
  section2FirstLot: function(){
    return Section2FirstLot.find();
  }
})

Template.section2FirstLot.onCreated(function(){
   if (! Meteor.userId()){
    Router.go('login');
  }
  else {
  Session.setDefault("secondLotcounter", 0);
  }
});

Template.section2FirstLot.events({
  'click .button.success': function(event){
   
   //define the counter variables
    var c = 0; var d = 0; var e = 0;
    var f = 0; var g = 0; var h = 0;
    var i = 0; var j = 0;
    
    //define keyword arrays
    var question3aKeywordArray = ['item', 'reused', 'less', 'energy', 'recycle', 'extract', 'materials', 'new', 'landfills', 'reduces', 'improves', 'image', 'organisation',
                                 'cheaper', 'competitive', 'sorted', 'different', 'categories', 'limited', 'amount', 'inferior', 'quality'];
    
    var question3bKeywordArray = ['tied', 'up', 'improve', 'deteriorate', 'high', 'wastage', 'theft', 'loss', 'profit', 'unsold', 'goods', 'storage', 'insurance', 
                                 'obselete', 'wastes', 'money'];
    
    var question3cPart1KeywordArray = ['circles', 'benchmarking', 'assurance', 'management', 'control', 'standards', 'inputs', 'materials', 'training', 'maintenance'];
    
    var question3cPart2KeywordArray = ['small', 'group', 'meet', 'improve', 'methods', 'working', 'match', 'standard', 'competitor', 'ensure', 'prevent', 'errors', 
                                      'checking', 'stage', 'production', 'continuous', 'responsibility', 'consistent', 'beginning', 'process', 'logo', 'confidence',
                                      'final', 'competent', 'maintained', 'mistakes'];
    
    var question4aKeywordArray = ['vary', 'not', 'do', 'income', 'received', 'sale', 'services' ];
    
    var question4bKeywordArray = ['supplier', 'wages', 'utilities', 'advertising', 'methods', 'premises', 'rent', 'budgeting', 'machinery', 'automation', 'leasing',
                                 'bulk', 'temporary', 'energy', 'saving', 'sensors', 'motion', 'website', 'email'];
    
    var question4cPart1KeywordArray = ['formulate', 'calculate', 'information', 'automatic', 'changes', 'reduces', 'error', 'saved', 'edited', 'tempaltes', 'financial', 
                                      'budgets', 'statements', 'standardisation', 'processes', 'replicated', 'graphs', 'charts', 'comparison'];
    
    var question4cPart2KeywordArray = ['processing', 'documents', 'departments', 'annual', 'budget', 'figure', 'compile', 'shareholders', 'financial', 'database', 'records',
                                      'suppliers', 'debtors', 'accounts', 'powerpoint', 'meeting', 'internet', 'website', 'online'];
    
    
    //get user input and store in meaningful variable names
    //convert to lower case so that input can match the keyword array and replace any malicious chars before input into the database
    var question3a = $('input[name=question3a]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var question3b = $('input[name=question3b]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var question3cPart1 = $('input[name=question3cPart1]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var question3cPart2 = $('input[name=question3cPart2]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var question4a = $('input[name=question4a]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var question4b = $('input[name=question4b]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var question4cPart1 = $('input[name=question4cPart1]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var question4cPart2 = $('input[name=question4cPart2]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    
//define marking functions that can be used over and over again 
//also handy that if any of the logic needs changed which it no doubt will, only have to change it in the one place.
//first function is used to mark the questions that can award up to 2 marks  
//second function is used to mark the questions that can award only 1 mark 
//third function is used to mark the questions that can award up to 3 marks 
//forth function is used to mark the questions that can award up to 4 marks    
    var markTheQuestion = function(c, userInput, keywordArray){
    var c = c;
    var userInput = userInput;
    var keywordArray = keywordArray;  
    
    var splitUserInput = userInput.split(" ");
    
    $.each(keywordArray, function(indexA, valueA){
    $.each(splitUserInput, function(indexB, valueB){
    if(valueA == valueB){
    //if the value is found, log to the console that we have a match, add 1 to the variable C declared at the start and then break out of the loop.
    console.log('we have 1 skilled match');
    c++;     
    return false;        
    } 
    });  
    })
    
    //if c is not equal to 0 then set the session counter to 1, meaning that 1 mark has been given for this question.
    if (c >= 2){Session.set("secondLotcounter", Session.get("secondLotcounter") + 2);}
    else if (c != 2 && c != 0){Session.set("secondLotcounter", Session.get("secondLotcounter") + 1);}  
    };
   
    var markTheQuestion2 = function(c, userInput, keywordArray){
    var c = c;
    var userInput = userInput;
    var keywordArray = keywordArray;  
    
    var splitUserInput = userInput.split(" ");
    
    $.each(keywordArray, function(indexA, valueA){
    $.each(splitUserInput, function(indexB, valueB){
    if(valueA == valueB){
    //if the value is found, log to the console that we have a match, add 1 to the variable C declared at the start and then break out of the loop.
    console.log('we have 1 skilled match');
    c++;     
    return false;        
    } 
    });  
    })
    
    //if c is not equal to 0 then set the session counter to 1, meaning that 1 mark has been given for this question.
    if (c != 0){Session.set("secondLotcounter", Session.get("secondLotcounter") + 1);}  
    };
    
    var markTheQuestion3 = function(c, userInput, keywordArray){
    var c = c;
    var userInput = userInput;
    var keywordArray = keywordArray;  
    
    var splitUserInput = userInput.split(" ");
    
    $.each(keywordArray, function(indexA, valueA){
    $.each(splitUserInput, function(indexB, valueB){
    if(valueA == valueB){
    //if the value is found, log to the console that we have a match, add 1 to the variable C declared at the start and then break out of the loop.
    console.log('we have 1 skilled match');
    c++;     
    return false;        
    } 
    });  
    })
    
    //if c is not equal to 0 then set the session counter to 1, meaning that 1 mark has been given for this question.
    if (c >= 3){Session.set("secondLotcounter", Session.get("secondLotcounter") + 3);}
    else if (c != 3 && c == 2){Session.set("secondLotcounter", Session.get("secondLotcounter") + 2);}
    else if (c != 3 && c != 2 && c != 0){Session.set("secondLotcounter", Session.get("secondLotcounter") + 1);}  
    };
    
    var markTheQuestion4 = function(c, userInput, keywordArray){
    var c = c;
    var userInput = userInput;
    var keywordArray = keywordArray;  
    
    var splitUserInput = userInput.split(" ");
    
    $.each(keywordArray, function(indexA, valueA){
    $.each(splitUserInput, function(indexB, valueB){
    if(valueA == valueB){
    //if the value is found, log to the console that we have a match, add 1 to the variable C declared at the start and then break out of the loop.
    console.log('we have 1 skilled match');
    c++;     
    return false;        
    } 
    });  
    })
    
    //if c is not equal to 0 then set the session counter to 1, meaning that 1 mark has been given for this question.
    if (c >= 4){Session.set("secondLotcounter", Session.get("secondLotcounter") + 4);}
    else if (c < 4 && c > 2){Session.set("secondLotcounter", Session.get("secondLotcounter") + 3);}
    else if (c < 4 && c != 3 && c == 2){Session.set("secondLotcounter", Session.get("secondLotcounter") + 2);}
    else if (c < 4 && c != 3 && c != 2 && c != 0){Session.set("secondLotcounter", Session.get("secondLotcounter") + 1);}  
    };  
        
    //mark the questions and insert answers into database
    markTheQuestion4(c, question3a, question3aKeywordArray);
    markTheQuestion(d, question3b, question3bKeywordArray);
    markTheQuestion(e, question3cPart1, question3cPart1KeywordArray);
    markTheQuestion(f, question3cPart2, question3cPart2KeywordArray);
    markTheQuestion3(g, question4a, question4aKeywordArray);
    markTheQuestion3(h, question4b, question4bKeywordArray);
    markTheQuestion(i, question4cPart1, question4cPart1KeywordArray);
    markTheQuestion(j, question4cPart2, question4cPart2KeywordArray);
    
    var userID = Meteor.userId();
    var section2FirstLotTotal = Session.get("secondLotcounter");
    Session.set("currentTotal", Session.get('currentTotal') + section2FirstLotTotal);
    var percentage = "75%";
    var pastPaperId = Session.get('currentPastPaperId');
    var overallTotal = Session.get('currentTotal');
    
    Meteor.call('updatePastPaperWithSection2FirstLotAnswers', overallTotal, pastPaperId, question3a, question3b, question3cPart1, question3cPart2, 
                question4a, question4b, question4cPart1, question4cPart2, percentage);
    
    Meteor.defer(function() { Router.go('section2LastLot'); }) 
  }  
});