Template.caseStudy2Review.helpers({
  caseStudy2Answers: function(){
  var pastPaperId = Session.get('currentPastPaperId');
  return UserPastPapers.find({ _id: pastPaperId });
  }
});

Template.caseStudy2Review.onCreated(function(){
   if (! Meteor.userId()){
    Router.go('login');
  }
});