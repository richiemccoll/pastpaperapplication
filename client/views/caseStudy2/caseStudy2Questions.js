Template.caseStudy2Questions.helpers({
  caseStudy2Questions: function(){
  return CaseStudy2Questions.find();  
  }  
});

Template.caseStudy2Questions.onCreated(function(){
   if (! Meteor.userId()){
    Router.go('login');
  }
  else {
  //set a session counter variable to 0 when the page is created, this will be used for tracking the overall mark for this question
  Session.setDefault("question2counter", 0);
  }
});

Template.caseStudy2.onCreated(function(){
   if (! Meteor.userId()){
    Router.go('login');
  }  
});

Template.caseStudy2Questions.events({
  'click .button.success': function(){
    //defining counter variables
    var c = 0; var d = 0; var e = 0;
    var f = 0; var g = 0; var h = 0;
    var i = 0;
    
    //define keywords array for each question
    var question2AKeywordArray = ['reduce', 'homelessness', 'service', 'community', 'legitimate', 'income', 'provide', 'benefits', 'awareness', 'socially', 'responsible'];
    
    var question2BPart1KeywordArray = ['celebrity', 'endorsements', 'contributions'];
    
    var question2BPart2KeywordArray = ['free', 'complimentary', 'discount', 'limited', 'competition', 'prize', 'endorsement', 'fundraiser', 'advertising',
                                      'producing', 'audio', 'visual', 'adverts', 'commercial', 'catchy', 'local', 'national', 'radio', 'newspaper',
                                      'magazine', 'papers', 'outdoor', 'billboards', 'transport', 'website'];
    
    var question2CKeywordArray = ['political', 'changes', 'law', 'prevent', 'magazine', 'publishing', 'certain', 'local', 'council', 'vendors', 'licences', 
                                 'economic', 'reduction', 'consumer', 'spending', 'recession', 'inflation', 'increase', 'social', 'sympathy', 'homelessness',
                                 'technological', 'electronic', 'growth', 'decrease', 'demand', 'environmental', 'weather', 'snow', 'road', 'works', 'delivery',
                                 'distribution', 'late', 'competition', 'charity', 'organizations'];
    
    var question2DPart1KeywordArray = ['air', 'plane', 'road', 'van', 'lorry', 'car', 'sea', 'boat', 'pipeline', 'electronic'];
    
    var question2DPart2KeywordArray = ['provides', 'fast', 'transportation', 'worldwide', 'affected', 'weather', 'delays', 'expensive', 'direct', 'not', 'delivery', 'airport',
                                      'depart', '24/7', 'restrictions', 'hours', 'lorry', 'petrol', 'prices', 'increase', 'environmentally', 'friendly', 'bulky', 'goods',
                                      'additional', 'haulage', 'slower'];
    
    var question2EKeywordArray = ['profit', 'cost', 'production', 'materials', 'labour', 'costs', 'magazines', 'competitors', 'up-market', 'higher', 'income', 'vendors', 'target', 
                                 'market', 'willing', 'break-even', 'demand'];
    
    //now I am taking the user input and storing in variables converting any capitals to lower case so that it will match the keyword array
    //replace any malicious chars before input into the database
    var compareObjectives = $('input[name=compareObjectives]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var methodOfPromotion = $('input[name=methodOfPromotion]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var describeOtherMethods = $('input[name=describeOtherMethods]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var externalFactors = $('input[name=externalFactors]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var anotherMethodOfDistribution = $('input[name=anotherMethodOfDistribution]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var advantagesAndDisAdvantages = $('input[name=advantagesAndDisAdvantages]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var settingPriceFactors = $('input[name=settingPriceFactors]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');


//define two global marking functions that can be used over and over again 
//also handy that if any of the logic needs changed which it no doubt will, only have to change it in the one place.
//first function is used to mark the questions that can award up to 2 marks  
//second function is used to mark the questions that can award only 1 mark 
//third function is used to mark the questions that can award up to 3 marks  
    var markTheQuestion = function(c, userInput, keywordArray){
    var c = c;
    var userInput = userInput;
    var keywordArray = keywordArray;  
    
    var splitUserInput = userInput.split(" ");
    
    $.each(keywordArray, function(indexA, valueA){
    $.each(splitUserInput, function(indexB, valueB){
    if(valueA == valueB){
    //if the value is found, log to the console that we have a match, add 1 to the variable C declared at the start and then break out of the loop.
    console.log('we have 1 skilled match');
    c++;     
    return false;        
    } 
    });  
    })
    
    //if c is not equal to 0 then set the session counter to 1, meaning that 1 mark has been given for this question.
    if (c >= 2){Session.set("question2counter", Session.get("question2counter") + 2);}
    else if (c != 2 && c != 0){Session.set("question2counter", Session.get("question2counter") + 1);}  
    };
    var markTheQuestion2 = function(c, userInput, keywordArray){
    var c = c;
    var userInput = userInput;
    var keywordArray = keywordArray;  
    
    var splitUserInput = userInput.split(" ");
    
    $.each(keywordArray, function(indexA, valueA){
    $.each(splitUserInput, function(indexB, valueB){
    if(valueA == valueB){
    //if the value is found, log to the console that we have a match, add 1 to the variable C declared at the start and then break out of the loop.
    console.log('we have 1 skilled match');
    c++;     
    return false;        
    } 
    });  
    })
    
    //if c is not equal to 0 then set the session counter to 1, meaning that 1 mark has been given for this question.
    if (c != 0){Session.set("question2counter", Session.get("question2counter") + 1);}  
    };   
    var markTheQuestion3 = function(c, userInput, keywordArray){
    var c = c;
    var userInput = userInput;
    var keywordArray = keywordArray;  
    
    var splitUserInput = userInput.split(" ");
    
    $.each(keywordArray, function(indexA, valueA){
    $.each(splitUserInput, function(indexB, valueB){
    if(valueA == valueB){
    //if the value is found, log to the console that we have a match, add 1 to the variable C declared at the start and then break out of the loop.
    console.log('we have 1 skilled match');
    c++;     
    return false;        
    } 
    });  
    })
    
    //if c is not equal to 0 then set the session counter to 1, meaning that 1 mark has been given for this question.
    if (c >= 3){Session.set("question2counter", Session.get("question2counter") + 3);}
    else if (c != 3 && c == 2){Session.set("question2counter", Session.get("question2counter") + 2);}
    else if (c != 3 && c != 2 && c != 0){Session.set("question2counter", Session.get("question2counter") + 1);}  
    };
 
    
    //call the marking function(s) here
    markTheQuestion(c, compareObjectives, question2AKeywordArray);
    markTheQuestion2(d, methodOfPromotion, question2BPart1KeywordArray);
    markTheQuestion(e, describeOtherMethods, question2BPart2KeywordArray);
    markTheQuestion3(f, externalFactors, question2CKeywordArray);
    markTheQuestion2(g, anotherMethodOfDistribution, question2DPart1KeywordArray);
    markTheQuestion3(h, advantagesAndDisAdvantages, question2DPart2KeywordArray);
    markTheQuestion3(i, settingPriceFactors, question2EKeywordArray);
    
    var userID = Meteor.userId();
    var question2Total = Session.get("question2counter");
    Session.set("currentTotal", Session.get('currentTotal') + question2Total);
    var percentage = '50%';
    var pastPaperId = Session.get('currentPastPaperId');
    var overallTotal = Session.get('currentTotal');
    
    Meteor.call('updatePastPaperWithQ2Answers', overallTotal, pastPaperId, compareObjectives, methodOfPromotion, describeOtherMethods, externalFactors, anotherMethodOfDistribution,
               advantagesAndDisAdvantages, settingPriceFactors, percentage);
    
    Meteor.defer(function() { Router.go('caseStudy2Review'); }) 
  }
});