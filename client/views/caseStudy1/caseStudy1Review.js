Template.caseStudy1Review.helpers({
  caseStudy1Answers: function(){
  var pastPaperId = Session.get('currentPastPaperId');
  return UserPastPapers.find({ _id: pastPaperId });
},
  caseStudy1AnswerHints: function(){
    return CaseStudy1AnswerHints.find().fetch();
  }
});

Template.caseStudy1Review.onCreated(function(){
  if (! Meteor.userId()){
    Router.go('login');
  }
});

Template.caseStudy1.onCreated(function(){
   if (! Meteor.userId()){
    Router.go('login');
  }
});