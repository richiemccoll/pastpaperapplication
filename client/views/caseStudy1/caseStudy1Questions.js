Template.caseStudy1Questions.helpers({
  caseStudy1Questions: function(){
    return CaseStudy1Questions.find();
  }
});

Template.caseStudy1Questions.onCreated(function(){
  if (! Meteor.userId()){
    Router.go('login');
  }
  else {
  //set a session counter variable to 0 when the page is created, this will be used for tracking the overall mark for this question
  Session.setDefault("counter", 0);
  }
});

Template.caseStudy1Questions.events({
  'click .button.success': function(event){
    //set internal variables to help inside the each jquery loop(s)
    var c = 0; var d = 0; var e = 0; var f = 0; 
    var g = 0; var h = 0; var i = 0; var j = 0; 
    var k = 0; var l = 0; var m = 0; var n = 0;
    var o = 0; var p = 0; var q = 0;

//define a global marking function that can be used over and over again 
//also handy that if any of the logic needs changed which it no doubt will, only have to change it in the one place.
    var markTheQuestion = function(c, userInput, keywordArray){
    var c = c;
    var keywordArray = keywordArray;  

    var splitUserInput = userInput.split(" ");
    console.log(userInput);   
    
    $.each(keywordArray, function(indexA, valueA){
    $.each(splitUserInput, function(indexB, valueB){
    if(valueA == valueB){
    //if the value is found, log to the console that we have a match, add 1 to the variable C declared at the start and then break out of the loop.
    console.log('we have 1 skilled match'); 
    c++;     
    return false;        
    } 
    });  
    })
    
    //if c is not equal to 0 then set the session counter to 1, meaning that 1 mark has been given for this question.
    if (c != 0){Session.set("counter", Session.get("counter") + 1);}
    };
    
//question 1 A Part 1    
//get all of the user input and instantiate the keyword arrays
//take the user input, convert it to lower case so it matches the key word array and replace any malicious chars before input into the database   
    var firstSkill = $('input[name=firstSkill]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var secondSkill = $('input[name=secondSkill]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var question1Apart1Array = ['completed', 'train', 'experience', 'managing', 'communication', 'plan', 'decision', 'financial', 'risk', 'idea', 'creative'];

//call the global marking function passing the neccessary arguments
    markTheQuestion(c, firstSkill, question1Apart1Array);
    markTheQuestion(d, secondSkill, question1Apart1Array);

// //question 1 A Part 2
    var howDoesFirstSkillHelp = $('input[name=howDoes1stSkillHelp]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var howDoesSecondSkillHelp = $('input[name=howDoes2ndSkillHelp]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');

    var question1Apart2Array = ['training', 'service', 'provide', 'clients', 'manage', 'bookings', 'finance', 'effectively', 'experienced', 'customer', 'needs',
                               'communication', 'skills', 'build', 'relationships', 'business', 'advisor', 'planning', 'risk', 'failure', 'finance', 'overspending'];    

    markTheQuestion(e, howDoesFirstSkillHelp, question1Apart2Array);
    markTheQuestion(f, howDoesSecondSkillHelp, question1Apart2Array);
  
////question 1 B
    var onetypeOfFinance = $('input[name=1typeOfFinance]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var anotherTypeOfFinance = $('input[name=anotherTypeOfFinance]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');

    var question1BArray = ['grant', 'repaid', 'loan', 'money', 'interest', 'incurred', 'added', 'owed', 'one off', 'payment', 
                          'several', 'finance', 'external', 'sources'];
    
    markTheQuestion(g, onetypeOfFinance, question1BArray);
    markTheQuestion(h, anotherTypeOfFinance, question1BArray);
 
// //question 1 C
    var oneImportanceOfGoodCustomerService = $('input[name=1importanceOfGoodCustomerService]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var anotherImportanceOfGoodCustomerService = $('input[name=anotherImportanceOfGoodCustomerService]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');

    var question1CArray = ['customers', 'return', 'increase', 'sales', 'reputation', 'new', 'higher', 'prices', 'recommend', 'less', 'complaints'];
    
    markTheQuestion(i, oneImportanceOfGoodCustomerService, question1CArray);
    markTheQuestion(j, anotherImportanceOfGoodCustomerService, question1CArray);

// //question 1 D part 1
    var oneStageOfRecruitmentProcess = $('input[name=1stageOfRecruitmentProcess]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var anotherStageOfRecruitmentProcess = $('input[name=anotherStageOfRecruitmentProcess]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var andAnotherStageOfRecruitmentProcess = $('input[name=andAnotherStageOfRecruitmentProcess]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');

    var question1DPart1Array = ['identify', 'vacancy', 'analysis', 'description', 'specification', 'advertise', 'send', 'forms'];
    
    markTheQuestion(k, oneStageOfRecruitmentProcess, question1DPart1Array);
    markTheQuestion(l, anotherStageOfRecruitmentProcess, question1DPart1Array);
    markTheQuestion(m, andAnotherStageOfRecruitmentProcess, question1DPart1Array);
    
////question 1 D part 2
    var oneFeatureOfEqualityAct = $('input[name=1featureOfEqualityAct]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var anotherFeatureOfEqualityAct = $('input[name=anotherFeatureOfEqualityAct]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
        
    var question1DPart2Array = ['simplifies', 'current', 'discrimination', 'legislation', 'protected', 'characteristics', 'victimisation', 'harassment', 'bullying'];
    
    markTheQuestion(n, oneFeatureOfEqualityAct, question1DPart2Array);
    markTheQuestion(o, anotherFeatureOfEqualityAct, question1DPart2Array);

////question 1 E part 1 and 2
    var productLifeCycleStage = $('input[name=productLifeCycleStage]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');
    var describeStage = $('input[name=describeStage]').val().toLowerCase().replace(/[&\/\\@=#,+()$~%.'":*?<>{}]/g,'_');

    var question1EArray = ['growth', 'maturity', 'awareness', 'product', 'increases', 'grow', 'reached', 'peak'];  

    markTheQuestion(p, productLifeCycleStage, question1EArray);
    markTheQuestion(q, describeStage, question1EArray);
    
    var userID = Meteor.userId();
    var question1Total = Session.get("counter");
    var percentage = '25%';
        
    var pastPaperId = Session.get('currentPastPaperId');
    Session.set('currentTotal', question1Total);
    
    //call a meteor method to update the past paper with the case study 1 answers passing in all the variables defined in this click function.
    Meteor.call('updatePastPaperWithQ1Answers', question1Total, pastPaperId, firstSkill, secondSkill, howDoesFirstSkillHelp, howDoesSecondSkillHelp, onetypeOfFinance, anotherTypeOfFinance,
                                                oneImportanceOfGoodCustomerService, anotherImportanceOfGoodCustomerService,
                                                oneStageOfRecruitmentProcess, anotherStageOfRecruitmentProcess, andAnotherStageOfRecruitmentProcess, oneFeatureOfEqualityAct,
                                                anotherFeatureOfEqualityAct, productLifeCycleStage, describeStage, percentage);
    
    Meteor.defer(function() { Router.go('caseStudy1Review'); })    
  }
});