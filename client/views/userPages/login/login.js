Template.login.onRendered(function(){
$('.login').validate({
submitHandler: function(event){
var username = $('[name=username]').val();
var password = $('[name=password]').val();

var isValidInput = function(u, p){
  var u = u;
  var p = p;
  var numbersAndLetters = /^[0-9a-zA-Z]+$/;
  if (u.match(numbersAndLetters) && p.match(numbersAndLetters))
    {
      Meteor.loginWithPassword(username, password, function(error){
      if(error){
      console.log(error.reason);
      } 
      else {
      Router.go("/userOverview");
    }
    });
    }
  else {
    alert('Please make sure that the username and password are both in the correct format.')
  }
}  

    isValidInput(username, password);
  
}
});
});

if (Meteor.isClient){
$.validator.setDefaults({
rules: {
            password: {
                required: true,
                minlength: 6
            }
        },
messages: {
            username: "You must use a valid username. If you can't remember it, sign up again. It's free. "
        }  
});
}

Template.main.events({
  'click .top-bar .button': function(){
    var c = window.confirm('Are you sure you want to log out?');
    if (c == true){  
    Meteor.logout();
    Meteor.defer(function() { Router.go('login'); })  
    }
    else {
      //do nothing when the user clicks cancel
    }
  }
})