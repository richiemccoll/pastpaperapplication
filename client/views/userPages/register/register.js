Template.register.events({
    'submit form': function(event){
        event.preventDefault();
        var email = $('[name=email]').val();
        var username = $('[name=username]').val();
        var password = $('[name=password]').val();
        
      //function that checks whether the user input is in email address format, username/password with only a-z and 1-9 characters
        var isValidInput = function(e, u, p){
          var pattern = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
          var numbersAndLetters = /^[0-9a-zA-Z]+$/;
          var e = e;
          var u = u;
          var p = p;
          if (e.match(pattern) && u.match(numbersAndLetters) && p.match(numbersAndLetters))
          { 
          //only create the user account if the input is validated against proper regex
            Accounts.createUser({
                    email: email,
                    password: password,
                    username: username
          });
          Router.go('userOverview');  
          }
          else { alert('Email address must be a valid email & format. Username and Password must be letters and numbers only. Please check these to reflect this.'); }
        }
        
        isValidInput(email, username, password);
        
    }
});
