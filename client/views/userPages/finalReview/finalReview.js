Template.finalReview.helpers({
  pastPaper: function(){
  var pastPaperId = Session.get('currentPastPaperId');
  return UserPastPapers.find({ _id: pastPaperId });
  }
});

Template.finalReview.onCreated(function(){
  if (! Meteor.userId()){
    Router.go('login');
  }
});

Template.finalReview.events({
  'click .button.success': function(event){
    Router.go('userOverview');
  }
})