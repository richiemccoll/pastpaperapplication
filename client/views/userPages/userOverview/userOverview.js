Template.userOverview.events({
  'click .button.tiny': function(event){
   Meteor.call('addPastPaper'); 
  },
  'click .button.success': function(event){
    var pastPaperId = this._id;
    var currentTotal = this.overallMark;
    Session.set('currentTotal', currentTotal);
    Session.set('currentPastPaperId', pastPaperId);
    var percentage = this.percentage;
    console.log(percentage);
    console.log(currentTotal);
    if (percentage == "25%") Meteor.defer(function() {Router.go('caseStudy2');})
    if (percentage == "0%") Meteor.defer(function() {Router.go('caseStudy1');})
    if (percentage == "50%") Meteor.defer(function() {Router.go('section2FirstLot');})
    if (percentage == "75%") Meteor.defer(function() {Router.go('section2LastLot');})
    if (percentage == "100%") Meteor.defer(function() {Router.go('finalOverview');})
  }
});

Template.userOverview.helpers({
  returnPastPapers: function(){
    var userId = Meteor.userId();
    return UserPastPapers.find({userId: userId});
  }
});

Template.userOverview.onCreated(function(){
  if (! Meteor.userId()){
    Router.go('login');  
  }
  else {
   Session.setDefault('currentPastPaperId', 0);
  }
  });