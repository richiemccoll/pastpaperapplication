PastPapers = new Meteor.Collection('PastPapers');

PastPaper = function(id, name, owner){
  this._id = id;
  this._name = name;
  this._owner = owner;
};

PastPaper.prototype = {
    get id() {
        // readonly
        return this._id;
    },
    get owner() {
        // readonly
        return this._owner;
    },
    get name() {
        return this._name;
    },

    set name(value) {
        this._name = value;
    },
    save: function() { 
    var that = this;
    var doc = {name: this.name};
      
    PastPapers.insert(doc, function(error, result) {
            that._id = result;
        });  
    }
};